/* Create a simple banking application
    There should be a bank class
    The bank should have an arraylist of branches
    Each branch should have an arraylist of customers
    The customer class should have an arraylist of Doubles (transation)
    Customer:
    Name and an ArrayList of Doubles
    Branch:
    Needs to be able to add a new customer and initial transaction amount
    Also need to add additional transations for that customer and branch
    Bank:
    Add a new branch
    Add a new customer to that branch with initial transaction
    Add a transaction for an existing customer for that branch
    Show a list of customers for a particular branch and optionally list their transactions
    Demonstrate auto boxing and unboxing in your code
    Add data validation
 */
public class Main
{
}
